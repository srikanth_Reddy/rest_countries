import React, { Component } from 'react'
import * as Api from './Api'
import './CountryDetails.css'
import { Link } from 'react-router-dom';

export default class CountryDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false
        }
    }


    componentDidMount() {
        Api.getSingleCountry(this.props.match.params.code)
            .then(res => this.setState({ countryData: res }), err => this.setState({ err }))
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.code !== this.props.match.params.code) {
            Api.getSingleCountry(this.props.match.params.code)
                .then(res => this.setState({ countryData: res }), err => this.setState({ err }))
        }

    }



    render() {
        if (this.state.err) {
            return <div>Loading Failed...</div>

        } else if(this.state.countryData){
            return (

                <div className="countryPage">
                    <div >

                        <div className='title'>
                            <h2>Where in the world</h2>
                            <div><button>Dark Mode</button></div>
                        </div>

                        <Link to='/'> <button className='btn'><span className='arrow'>&#8592;</span>Back</button></Link>

                        <div className='singleCountry'>
                            <div className='Countryflag'><img alt="flag" src={this.state.countryData.flags.png}></img></div>

                            <div className='details'>
                                <h1>{this.state.countryData.name}</h1>
                                <div className='countryDetails'>
                                    <div className='left'>
                                        <div><span className='name'> Native Name : </span>{this.state.countryData.nativeName} </div>
                                        <div><span className='name'> Population : </span>{this.state.countryData.population} </div>
                                        <div> <span className='name'> Region : </span>{this.state.countryData.region} </div>
                                        <div> <span className='name'> Sub Region : </span>{this.state.countryData.subregion} </div>
                                    </div>
                                    <div className='right'>
                                        <div> <span className='name'> Capital : </span>{this.state.countryData.capital} </div>
                                        <div><span className='name'> Top Level Domain : </span>{this.state.countryData.topLevelDomain[0]} </div>
                                        <div> <span className='name'> Currencies : </span>{(this.state.countryData.currencies || []).map((cur) => { return <span key={cur.code}>{cur.name},</span> })} </div>
                                        <div> <span className='name'> Languages : </span>{(this.state.countryData.languages || []).map((lng) => { return <span key={lng.iso639_1}>{lng.name},</span> })} </div>
                                    </div>
                                </div>
                                <div>
                                    <h4>Border Countries : {(this.state.countryData.borders || []).map((border) => { return <Link to={`/country/${border}`} style={{ textDecoration: "none" }} key={border}> <span className='footer'>{border}</span></Link> })}</h4>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            )
        }else{
            return <div>Loading...</div>
        }
    }
}

