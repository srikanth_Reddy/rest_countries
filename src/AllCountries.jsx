import React, { Component } from 'react'
import * as Api from './Api'
import './AllCountries.css'
import { Link } from 'react-router-dom'

export default class AllCountries extends Component {
    constructor(props) {
        super(props);

        this.state = {
            countries: [],
            isLoaded: false,
            err: false,
            region: "",
            searchStr: ""
        };
    }

    componentDidMount() {
        Api.getAllCountries().then(
            (res) => {
                this.setState({
                    ...this.state,
                    countries: res,
                    isLoaded: true
                })
            }, (err) => {
                this.setState({
                    ...this.state,
                    isLoaded: true,
                    err: true
                })
            }
        )
    }


    render() {

        let regions = () => {

            return Object.keys(this.state.countries.reduce((acc, cur) => {
                acc[cur.region] = 1;
                return acc;
            }, {}))
        }


        let filteredData = () => {
            if (this.state.region === '' && this.state.searchStr === "") {
                return this.state.countries;
            } else if (this.state.region !== '' && this.state.searchStr === "") {
                return this.state.countries.filter((country) => (country.region === this.state.region) ? true : false)
            } else if (this.state.region === '' && this.state.searchStr !== "") {
                return this.state.countries.filter((country) => ((country.name.toLowerCase()).replace(" ","")).includes((this.state.searchStr.toLowerCase()).replace(" ","")) ? true : false)
            } else {
                return this.state.countries.filter((country) => {
                    if ((country.region === this.state.region || this.state.region === "") && (((country.name.toLowerCase()).replace(" ","")).includes((this.state.searchStr.toLowerCase()).replace(" ","")))) {
                        return true;
                    }
                })
            }
        }

        if (!this.state.isLoaded) {
            return <div>Loading...</div>

        } else if (this.state.err) {
            return <div>Loading failed..</div>
            
        } else {
            return (
                <div >
                    <div className='header'  >

                        <div className='title'>
                            <h2>Where in the world</h2>
                            <div><button>Dark Mode</button></div>
                        </div>
                        <div className='filtering'>
                            <div> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                                 <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" /> </svg> 
                                 <input onChange={(e) => this.setState({ ...this.state, searchStr: e.target.value })} type="text" placeholder='Search for a country...'></input></div>
                            <div>
                                <select onChange={(e) => this.setState({ ...this.state, region: e.target.value })}>
                                    <option value="">All</option>
                                    {regions().map((region) => {
                                        return <option key={region} value={region}>{region}</option>
                                    })}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className='countries'>
                        {filteredData().map((country) => {
                            return <div key={country.name}><Link to={`/country/${country.alpha2Code}`} style={{ textDecoration: "none" }} ><div className='country' >
                                <img alt="flag" className='flag' src={country.flags.png}></img>
                                <div className='info'>
                                    <h3>{country.name}</h3>
                                    <div><span>Population :</span> {country.population}</div>
                                    <div><span>Region :</span> {country.region}</div>
                                    <div><span>Capital :</span> {country.capital}</div>
                                </div>
                            </div>
                            </Link>
                            </div>
                        })}
                    </div>
                </div>
            )
        }

    }
}
