import axios from "axios";


export const getAllCountries = () => {
    return axios
    .get("https://restcountries.com/v2/all")
    .then((res)=>res.data)
}

export const getSingleCountry = (code) => {
    return axios
    .get(`https://restcountries.com/v2/alpha/${code}`)
    .then(res => res.data)
}

