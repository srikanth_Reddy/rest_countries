import './App.css';
import AllCountries from './AllCountries';
import CountryDetails from './CountryDetails';

import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

export default class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={AllCountries}/>
             
          <Route path="/country/:code" component={CountryDetails}/>
            
        </Switch>
    </Router>
    )
  }
}



